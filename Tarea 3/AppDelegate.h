//
//  AppDelegate.h
//  Tarea 3
//
//  Created by Jorge Leonardo Monge García on 21/7/15.
//  Copyright (c) 2015 LumenUp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

